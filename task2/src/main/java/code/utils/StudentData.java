package code.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StudentData {
    public static void saveStudent(Student student) {
        try (Connection connection = DatabaseConnection.getConnection()) {
            String query = "INSERT INTO students (nim, name, major) VALUES (?, ?, ?)";
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                preparedStatement.setString(1, student.getNim());
                preparedStatement.setString(2, student.getName());
                preparedStatement.setString(3, student.getMajor());
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static List<Student> getAllStudents() {
        List<Student> studentList = new ArrayList<>();
        try (Connection connection = DatabaseConnection.getConnection()) {
            String query = "SELECT * FROM students";
            try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    String nim = resultSet.getString("nim");
                    String name = resultSet.getString("name");
                    String major = resultSet.getString("major");
                    Student student = new Student(nim, name, major);
                    studentList.add(student);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return studentList;
    }
}

