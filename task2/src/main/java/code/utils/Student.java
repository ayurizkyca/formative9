package code.utils;

import java.io.IOException;
import java.io.Serializable;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Student implements Serializable{
    private String name, nim, major;

    public Student() {
    }

    public Student(String nim, String name, String major) {
        this.name = name;
        this.nim = nim;
        this.major = major;
    }

    @Override
    public String toString() {
        return "{" +
            " name='" + getName() + "'" +
            ", nim='" + getNim() + "'" +
            ", major='" + getMajor() + "'" +
            "}";
    }    

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNim() {
        return this.nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getMajor() {
        return this.major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    // Serialisasi objek ke JSON
    public String toJson() {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.writeValueAsString(this);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    // Deserialisasi objek dari JSON
    public static Student fromJson(String json) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(json, Student.class);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
