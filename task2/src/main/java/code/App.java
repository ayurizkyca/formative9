package code;

import code.utils.DatabaseConnection;
import code.utils.Student;
import code.utils.StudentData;

import java.util.List;

public class App {

    public static void main(String[] args) {
        // Membuat beberapa objek Mahasiswa
        Student student1 = new Student("201831098", "Ayuraaa", "Computer Science");
        Student student2 = new Student("201831097", "Awaliaaa", "Mathematics");

        // Menyimpan Mahasiswa ke database
        StudentData.saveStudent(student1);
        StudentData.saveStudent(student2);

        // Mengambil dan menampilkan daftar Mahasiswa dari database
        List<Student> studentList = StudentData.getAllStudents();
        System.out.println("Daftar Mahasiswa:");
        for (Student student : studentList) {
            System.out.println(student);
        }

        // Menutup koneksi ke database (opsional, tergantung pada kebutuhan)
        try {
            DatabaseConnection.closeConnection(DatabaseConnection.getConnection());
        } catch (Exception e) {
            e.printStackTrace();
        }

         // Serialisasi objek Mahasiswa ke JSON
         String jsonStudent1 = student1.toJson();
         String jsonStudent2 = student2.toJson();
 
         System.out.println("JSON Mahasiswa 1: " + jsonStudent1);
         System.out.println("JSON Mahasiswa 2: " + jsonStudent2);
 
         // Deserialisasi objek Mahasiswa dari JSON
         Student deserializedStudent1 = Student.fromJson(jsonStudent1);
         Student deserializedStudent2 = Student.fromJson(jsonStudent2);
 
         System.out.println("Deserialized Mahasiswa 1: " + deserializedStudent1);
         System.out.println("Deserialized Mahasiswa 2: " + deserializedStudent2);
    }
}
