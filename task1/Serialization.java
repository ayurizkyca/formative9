package task1;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Serialization {
    public static void main(String[] args) {
        Student student1 = new Student("Putu Ayu", "201831085", "Teknik Informatika");

        //serialization
        serializePerson(student1);

        //deserialization
        Student deserialization = deserializePerson();
        System.out.println(deserialization);
    }

    public static void serializePerson(Student student) {
        String folderPath = "task1";
        File folder = new File(folderPath);
        try (ObjectOutputStream objectOut = new ObjectOutputStream(new FileOutputStream(folder + "/student.ser"))) {
            objectOut.writeObject(student);
            System.out.println("Student object has been serialized");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Student deserializePerson() {
        String folderPath = "task1";
        File folder = new File(folderPath);
        try (ObjectInputStream objectIn = new ObjectInputStream(new FileInputStream(folder + "/student.ser"))) {
            return (Student) objectIn.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
