package task1;

import java.io.Serializable;

public class Student implements Serializable{
    private String name, nim, major;

    public Student() {
    }

    public Student(String nim, String name, String major) {
        this.name = name;
        this.nim = nim;
        this.major = major;
    }

    @Override
    public String toString() {
        return "{" +
            " name='" + getName() + "'" +
            ", nim='" + getNim() + "'" +
            ", major='" + getMajor() + "'" +
            "}";
    }    

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNim() {
        return this.nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getMajor() {
        return this.major;
    }

    public void setMajor(String major) {
        this.major = major;
    }
}
